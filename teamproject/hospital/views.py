from django.shortcuts import render,redirect
from django.shortcuts import resolve_url
from django.urls import reverse_lazy
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.generic import *
from . import models
from qor.models import *
from . import forms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.

class HospitalIndex(ListView):
     #患者テーブル連携
    model = User
    # レコード情報をテンプレートに渡すオブジェクト
    context_object_name = "patient_list"
    # テンプレートファイル連携
    template_name = 'hospital/top.html'


class PatientIndex(ListView):
    #患者テーブル連携
    model = User
    # レコード情報をテンプレートに渡すオブジェクト
    context_object_name = "user_list"
    # テンプレートファイル連携
    template_name = 'hospital/patient_index.html'
    #　表示件数
    paginate_by = 10


class PatientDetail(DetailView):
    #患者テーブル連携
    model = User
    # テンプレートファイル連携
    template_name = 'hospital/patient_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = User.objects.get(pk=self.kwargs['pk'])
        #問診票ページネーション
        medical_sheets = Medicalsheet.objects.filter(user=user)
        paginator = Paginator(medical_sheets, 10)
        page = self.request.GET.get('page', 1)
        try:
    	    pages = paginator.page(page)
        except PageNotAnInteger:
    	    pages = paginator.page(1)
        except EmptyPage:
    	    pages = paginator.page(1)

        context['user'] = user
        context['page_obj'] = pages
        return context

#診断書Views処理

class ChartIndex(ListView):
    #患者テーブル連携
    model = Chart
    # レコード情報をテンプレートに渡すオブジェクト
    context_object_name = "chart_list"
    # テンプレートファイル連携
    template_name = 'hospital/chart_index.html'

    paginate_by = 10

class ChartDetail(DetailView):
    #患者テーブル連携
    model = Chart
    # テンプレートファイル連携
    template_name = 'hospital/chart_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        chart = Chart.objects.get(pk=self.kwargs['pk'])
        context['chart'] = chart
        context['medical_sheet'] = chart.medicalsheet
        context['user'] = chart.medicalsheet.user
        return context

class ChartCreate(CreateView):
    form_class = forms.ChartForm
    template_name = 'hospital/chart_form.html'

    '''
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)
    '''

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        medical_sheet = Medicalsheet.objects.get(pk=self.kwargs['pk'])
        context['medical_sheet'] = medical_sheet
        return context

    def get_form_kwargs(self):
        kwargs = super(ChartCreate, self).get_form_kwargs()
        kwargs['medicalsheet_id'] = self.kwargs['pk']
        return kwargs

    #更新後のリダイレクト先
    def get_success_url(self):
        medical_sheet = Medicalsheet.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('qor_app:patient_detail',kwargs={'pk':medical_sheet.user.id})

class ChartDelete(DeleteView):
    model = Chart 

    def get_success_url(self):
        chart = Chart.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('qor_app:patient_detail',kwargs={'pk':chart.medicalsheet.user.id})

class ChartUpdate(UpdateView):
    #form_class = forms.ChartForm
    success_url = reverse_lazy('qor_app:patient_index')
    template_name = 'hospital/chart_form.html'

    def get_form_kwargs(self):
        kwargs = super(ChartCreate, self).get_form_kwargs()
        kwargs['patient_id'] = self.kwargs['patient_pk']
        return kwargs

    #更新後のリダイレクト先
    def get_success_url(self):
        return resolve_url('qor_app:patient_detail')


#class MedicalSheetUpdate(UpdateView):
