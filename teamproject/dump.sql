PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "QoR_patient" ("id" serial PRIMARY KEY AUTOINCREMENT, "patient_name" varchar(20) NOT NULL, "sickness" text NOT NULL, "allergy" text NOT NULL, "infectious_info" text NOT NULL, "combined_taboo" text NOT NULL, "register_date" datetime NOT NULL, "update_date" datetime NOT NULL, "date_joined" datetime NOT NULL, "email" varchar(254) NOT NULL, "first_name" varchar(150) NOT NULL, "is_active" bool NOT NULL, "is_staff" bool NOT NULL, "is_superuser" bool NOT NULL, "last_login" datetime NULL, "last_name" varchar(150) NOT NULL, "password" varchar(128) NOT NULL, "username" varchar(150) NOT NULL UNIQUE);
INSERT INTO QoR_patient VALUES(1,'','','','','','2022-10-10 14:42:47.154454','2022-10-10 14:42:47.154454','2022-10-10 14:42:47.154454','admin@example.com','',1,1,1,'2022-10-10 14:43:16.363970','','pbkdf2_sha256$260000$LAybp5SqXfXfCYb78aFroG$2gflvP7zC7GYRIP18fYKJkhZ4dGmR4eL5JSk8dCffPA=','admin');
INSERT INTO QoR_patient VALUES(2,'患者氏名','傷病名','アレルギー情報','感染症情報','禁忌情報','2022-10-10 14:43:38','2022-10-10 14:43:38','2022-10-10 14:43:38','user01@example.com','患者名',1,0,0,'2022-10-10 14:43:51','患者姓','Ibuki1010','患者1');
COMMIT;
