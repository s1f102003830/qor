from .base import *

ALLOWED_HOSTS = []
DEBUG = env('DEBUG')
SECRET_KEY = env('SECRET_KEY')


STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / 'static'
STATICFILE_DIRS = [
    BASE_DIR / "static",
]
