from django.apps import AppConfig


class QorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'qor'
