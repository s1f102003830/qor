from .models import Medicalsheet
from django import forms

class Med_sheet_form(forms.ModelForm):
    class Meta:
        model = Medicalsheet
        fields =('name','name2','birthday','reason', 'since_date', 'had_illnesses',
        'medicine_taken', 'allergy1','other1')
        labels = {
            'name': 'お名前',
            'name2':'フリガナ',
            'birthday':'生年月日:(入力例:2020-01-01)',
            'reason':"来院した理由は",
            'since_date':"症状はいつから:(入力例:2020-01-01)",
            'had_illnesses':"お持ちの病気等",
            'medicine_taken':"服用されている薬等",
            'allergy1':"アレルギー等",
            'other1':"その他",
        }
