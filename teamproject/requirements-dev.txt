
certifi==2022.6.15
colorama==0.4.5
distlib==0.3.6
dj-database-url==1.0.0
Django @ file:///tmp/build/80754af9/django_1625585912945/work
django-cors-headers==3.13.0
django-environ==0.9.0
django-filter==22.1
django-rest-multiple-models==2.1.3
djangorestframework==3.13.1
filelock==3.8.0
Markdown==3.4.1
Pillow==9.2.0
pipenv==2022.10.9
platformdirs==2.5.2
psycopg2 @ file:///C:/ci_310/psycopg2_1642085998310/work
pytz @ file:///C:/Windows/TEMP/abs_90eacd4e-8eff-491e-b26e-f707eba2cbe1ujvbhqz1/croots/recipe/pytz_1654762631027/work
qrcode==7.3.1
sqlparse @ file:///tmp/build/80754af9/sqlparse_1602184451250/work
typing_extensions @ file:///C:/Windows/TEMP/abs_dd2d0moa85/croots/recipe/typing_extensions_1659638831135/work
virtualenv==20.16.5
virtualenv-clone==0.5.7
wincertstore==0.2
