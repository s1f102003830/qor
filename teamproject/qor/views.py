from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import Http404
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.views.generic import DetailView,ListView
from . import models
from PIL import Image
import qrcode
import base64
from io import BytesIO
from .forms import Med_sheet_form
from qor.models import Medicalsheet, Patient
from django.http import StreamingHttpResponse
from django.views.decorators import gzip
import cv2
import webbrowser
import threading
from django.views import View
from config.settings import settings

#BASE_URL = 'http://127.0.0.1:8000/qor'
BASE_URL = settings.BASE_URL


#model実装時使用
#from QoR.models import 

# Create your views here.
def index(request):
    return render(request, 'QoR/index.html', {})

def explanation(request):
    return render(request,'QoR/explanation.html',{})

@login_required
def user_result(request, pk):
    user = request.user
    if user.id != pk:
        return render(request,'QoR/index.html',{})

    medsheet = Medicalsheet.objects.filter(user=request.user)

    img = qrcode.make( BASE_URL + 'hospital/' + str(pk))
    buffer = BytesIO()
    img.save(buffer, format="PNG")
    qr = base64.b64encode(buffer.getvalue()).decode().replace("'", "")
    param = { 
        'qr': qr,
        "medsheet":medsheet,
    }
    return render(request, 'QoR/user/user_result.html', param)

@login_required
def create_medsheet(request):
    message = ''
    if (request.method == 'POST'):
        form = Med_sheet_form(request.POST)
        if form.is_valid():
            medicalsheet = form.save(commit=False)
            medicalsheet.user = request.user
            form.save()
            
            return redirect(to='index')
        else:
            message = "再入力してください"

    context = {
        'message': message,
        'form':Med_sheet_form(
            initial = {
                'name':request.user.last_name
            }
        ),
    }
    return render(request, 'QoR/user/create_medsheet.html',context)

@login_required
def model_check(request):
    medcal_sheets = Medicalsheet.objects.filter(user=request.user)
    context = {
        'title':'medical sheet',
        'medcal_sheets':medcal_sheets,
    }
    return render(request, 'QoR/user/model_check.html', context)

# ストリーミング画像を定期的に返却するview
def video_feed_view():
    a = StreamingHttpResponse(generate_frame(), content_type='multipart/x-mixed-replace; boundary=frame')
    if(a):
        return lambda _: a
    else:
        url = a
        redirect(url)
    
# フレーム生成・返却する処理
def generate_frame():
    capture = cv2.VideoCapture(0)  # USBカメラから

    while True:
        if not capture.isOpened():
            print("Capture is not opened.")
            break
        # カメラからフレーム画像を取得
        ret, frame = capture.read()
        if ret:
            for d in decode(frame):
                s = d.data.decode()
                print(s)
                if (s):
                    url = s
                    webbrowser.open_new(url)
                    capture.release()

                frame = cv2.rectangle(frame, (d.rect.left, d.rect.top),
                                  (d.rect.left + d.rect.width, d.rect.top + d.rect.height), (0, 255, 0), 3)
                frame = cv2.putText(frame, s, (d.rect.left, d.rect.top + d.rect.height),
                                cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

        if not ret:
            print("Failed to read frame.")
            break
        # フレーム画像バイナリに変換
        ret, jpeg = cv2.imencode('.jpg', frame)
        byte_frame = jpeg.tobytes()


        # フレーム画像のバイナリデータをユーザーに送付する
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + byte_frame + b'\r\n\r\n')
    capture.release()


def hospital_top(request):
    return render(request,'QoR/hospital/top.html',{})
