from django.db import models
from django.utils import timezone
from django.conf import settings

# Create your models here

#病院マスタ
class Hospital(models.Model):
    hospital_id = models.CharField(max_length=10,primary_key=True)
    hospital_name = models.TextField()

#医師マスタ
class Docter(models.Model):
    hospital = models.ForeignKey(Hospital,on_delete=models.CASCADE)
    user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)

