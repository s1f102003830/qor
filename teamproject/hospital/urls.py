from django.urls import path
from . import views

app_name = 'qor_app'

# QoR/urls.py
urlpatterns = [
    path('hospital/top',views.HospitalIndex.as_view(),name="hospital_index"),
    path('hospital/index', views.PatientIndex.as_view(),name="patient_index"), 
    path('hospital/<int:pk>', views.PatientDetail.as_view(),name="patient_detail"), 
    path('hospital/chart',views.ChartIndex.as_view(),name="chart_index"),
    path('hospital/chart/<int:pk>',views.ChartDetail.as_view(),name="chart_detail"),
    path('hospital/<int:pk>/chart/create',views.ChartCreate.as_view(),name="chart_create"),
    path('hospital/chart/<int:pk>/delete',views.ChartDelete.as_view(),name="chart_delete"),
    path('hospital/chart/<int:pk>/update',views.ChartUpdate.as_view(),name="chart_update"),
]
 