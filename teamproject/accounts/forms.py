from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

Patient = get_user_model()

class SignUpForm(UserCreationForm):
    email = forms.CharField(max_length=254, required=True, widget=forms.EmailInput())
    class Meta:
        model = User
        fields = (
            'username', 
            'first_name',
            'last_name', 
            'email',
            'password1',
            'password2'
        )
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['last_name'].widget.attrs['placeholder'] = '姓'
        self.fields['last_name'].widget.attrs['class'] = 'bg-blue-100 w-1/2 p-2'
        self.fields['first_name'].widget.attrs['placeholder'] = '名'
        self.fields['first_name'].widget.attrs['class'] = 'bg-blue-100 w-1/2 p-2 ml-2'
        self.fields['username'].widget.attrs['placeholder'] = 'ユーザネームを入力'
        self.fields['username'].widget.attrs['class'] = 'bg-blue-100 w-full text-center p-2'
        self.fields['email'].widget.attrs['placeholder'] = 'メールアドレスを入力'
        self.fields['email'].widget.attrs['class'] = 'bg-blue-100 w-full text-center p-2'
        self.fields['password1'].widget.attrs['placeholder'] = 'パスワードを入力'
        self.fields['password1'].widget.attrs['class'] = 'bg-blue-100 w-full text-center p-2'
        self.fields['password2'].widget.attrs['placeholder'] = '確認用パスワードを入力'
        self.fields['password2'].widget.attrs['class'] = 'bg-blue-100 w-full text-center p-2'


class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
       super().__init__(*args, **kwargs)
       self.fields['username'].widget.attrs['placeholder'] = 'ユーザネーム'
       self.fields['username'].widget.attrs['class'] = 'bg-blue-100 w-full text-center p-2'
       self.fields['password'].widget.attrs['placeholder'] = 'パスワード'
       self.fields['password'].widget.attrs['class'] = 'bg-blue-100 w-full text-center p-2'