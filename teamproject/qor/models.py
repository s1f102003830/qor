from email.policy import default
from unittest.util import _MAX_LENGTH
from django.db import models
from django.utils import timezone
from django.conf import settings

# Create your models here.

# 患者マスタ ログイン認証情報を含むカスタムユーザ
class Patient(models.Model):
    patient_name = models.CharField(max_length=20,default="伊吹")  #患者名
    sickness = models.TextField(max_length=100,default="傷病名") #傷病名
    allergy = models.TextField(max_length=100)  #アレルギー情報
    infectious_info = models.TextField(max_length=100)  #感染症情報
    combined_taboo = models.TextField(max_length=100)   #薬剤併用禁忌情報
    register_date = models.DateTimeField(default=timezone.now)  #登録日時
    update_date = models.DateTimeField(default=timezone.now)    #更新日時
    user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)

#問診票
class Medicalsheet(models.Model):
    visited_day = models.DateField(auto_now=False, auto_now_add=False, default = timezone.now)
    name = models.CharField(max_length = 30, default='')
    name2 = models.CharField(max_length = 50, default='')
    birthday = models.DateField(auto_now=False, auto_now_add=False, default = timezone.now)
    reason = models.TextField(max_length=100)
    since_date = models.DateField(auto_now=False, auto_now_add=False, default = timezone.now)
    had_illnesses = models.TextField(max_length=100)
    medicine_taken = models.TextField(max_length=100)
    allergy1 = models.TextField(max_length=100)
    other1 = models.TextField(max_length=100,null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    
    def __str__(self):
        return '<name:' + str(self.name) + ',' + 'created_by' + str(self.visited_day) + '>'

# 診断カルテ
class Chart(models.Model):
    prescription1 = models.CharField(max_length=50)     #処方箋1
    prescription2 = models.CharField(max_length=50)     #処方箋2
    prescription3 = models.CharField(max_length=50)     #処方箋3
    note = models.TextField()                           #備考
    diagnose_date = models.DateTimeField(default=timezone.now) #診断日時
    update_date = models.DateTimeField(default=timezone.now) #更新日時
    medicalsheet = models.OneToOneField(Medicalsheet,on_delete=models.CASCADE,null=True,blank=True)   #問診票ID
