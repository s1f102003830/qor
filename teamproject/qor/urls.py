from django.urls import path
from . import views

# QoR/urls.py
urlpatterns = [
    path('', views.index, name='index'),
    path('explanation', views.explanation, name='explanation'),
    path('<int:pk>', views.user_result , name='user_result'),
    path('create_medsheet',views.create_medsheet ,name='create_medsheet'),
    path('model_check', views.model_check, name="model_check"),
    path('video_feed', views.video_feed_view(), name="video_feed"),
]
