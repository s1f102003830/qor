from .base import *

BASE_DIR = Path(__file__).resolve().parent.parent

env = environ.Env()
env.read_env(os.path.join(BASE_DIR, '.env'))
#LOCAL_URL = env('LOCAL_URL')
BASE_URL = env('BASE_URL')

ALLOWED_HOSTS = env.list('ALLOWED_HOSTS')
#ALLOWED_HOSTS = []


DEBUG = env('DEBUG')
SECRET_KEY = env('SECRET_KEY')


STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / 'static'
STATICFILE_DIRS = [
    BASE_DIR / "static",
]
