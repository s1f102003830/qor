from .models import *
from qor.models import *
from django import forms

class ChartForm(forms.ModelForm):

    class Meta:
        model = Chart
        fields = (
            'prescription1',
            'prescription2',
            'prescription3',
            'note',
            'medicalsheet',
        )
        labels={
           'prescription1':'処方箋1',
           'prescription2':'処方箋2',
           'prescription3':'処方箋3',
           'note':'備考',
        }


    def __init__(self,*args,**kwargs):
        medicalsheet_id = kwargs.pop('medicalsheet_id')
        super(ChartForm,self).__init__(*args, **kwargs)
        self.fields['prescription1'].widget.attrs['placeholder'] = '処方箋を記入'
        self.fields['prescription1'].widget.attrs['class'] = 'border-none text-center w-full'
        self.fields['prescription2'].widget.attrs['placeholder'] = '処方箋を記入'
        self.fields['prescription2'].widget.attrs['class'] = 'border-none text-center w-full'
        self.fields['prescription3'].widget.attrs['placeholder'] = '処方箋を記入'
        self.fields['prescription3'].widget.attrs['class'] = 'border-none text-center w-full'
        self.fields['note'].widget.attrs['placeholder'] = '備考欄'
        self.fields['note'].widget.attrs['class'] = 'border-none w-full h-full'
        self.fields['medicalsheet'].initial = Medicalsheet.objects.get(pk=medicalsheet_id)
        self.fields['medicalsheet'].widget = forms.HiddenInput()
        self.fields['medicalsheet'].widget.attrs['class'] = 'border-none w-full hidden'