from django.utils import timezone
from django.contrib.auth.models import User
from qor.models import *
import traceback

if __name__ == '__main__':
    main()

def main():
    try:
        make_user()
        make_medicalsheet()
    except:
        traceback.print_exc()
        print("エラー")

#id100 ~ 120
def make_user():
    #ダミーデータの削除
    User.objects.filter(id__gte = 100).delete()
    #管理者ユーザ作成
    user = User(
        id = 999,
        is_superuser = 1,
        is_staff = 1,
        is_active = 1,
        username="admin",
        last_name = "管理者姓{0}".format(999),
        first_name = "管理者名{0}".format(999),
    )
    user.set_password("adminadmin")
    user.save()

    for i in range(100,120):
        User.objects.create(
            id = i,
            is_superuser = 0,
            username="ユーザー{0}".format(i),
            last_name = "ユーザ姓{0}".format(i),
            first_name = "ユーザ名".format(i),
            password = "Passw0rd!"
        )
    print("make_user compleate!!")

def make_patient():
    #ダミーデータの削除
    Patient.objects.filter(id__gte = 100).delete()
    
    for i in range(100,150):
        Patient.objects.create(
            id = i,
            patient_name = "患者{0}".format(i),
            sickness = "傷病名{0}".format(i),
            allergy = "アレルギー{0}".format(i),
            infectious_info = "感染症情報{0}".format(i),
            combined_taboo = "薬剤併用禁忌情報{0}".format(i),
            user_id = i
        )
    print("make_patient Compleate!!")


def make_medicalsheet():
    #ダミーデータの削除
    Medicalsheet.objects.filter(id__gte = 100).delete()

    #ユーザID100にデータ挿入
    #診断済みデータ
    for i in range(100,110):
        Medicalsheet.objects.create(
            id = i,
            reason ="理由{0}".format(i),
            user_id = 100
        )

    #未診断データ
    for i in range(111,120):
        Medicalsheet.objects.create(
            id = i,
            reason ="理由{0}".format(i),
            user_id = 100,
        )
    print("make_chart Compleate!!")

def make_chart():
    #ダミーデータの削除
    Chart.objects.filter(id__gte = 100).delete()
    
    for i in range(100,110):
        Chart.objects.create(
            id = i,
            prescription1 = "処方箋1-{0}".format(i),
            prescription2 = "処方箋2-{0}".format(i),
            prescription3 = "処方箋3-{0}".format(i),
            note = "備考{0}".format(i),
            medicalsheet_id = i
        )
    print("make_chart Compleate!!")