from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, logout,authenticate
from .forms import SignUpForm, LoginForm

# Create your views here.

def signup_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            print(user)
            login(request, user)
            return redirect('/QoR/')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})

def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request,data=request.POST)

        if form.is_valid():
            user = form.get_user()
            login(request,user)
            return redirect('/QoR/')

    else:
        form = LoginForm(request.POST)
    
    return render(request, 'registration/login.html', {'form': form,})

def logout_view(request):
    logout(request)

    return render(request, 'QoR/index.html')
